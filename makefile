PROGS := firstTest secondTest

CC=gcc
CXX=g++
CFLAGS += -Wall
CXXFLAGS += -Wall
LDFLAGS = libportaudio.a -lrt -lm -lasound -ljack -pthread

all: $(PROGS)


firstTest: testapp1.c 
	$(CC) $(CFLAGS) testapp1.c $(LDFLAGS) -o firstTest

secondTest: testapp2.c 
	$(CC) $(CFLAGS) testapp2.c $(LDFLAGS) -o secondTest

clean:
	rm -f $(PROGS) *.o *~ *.gch *.out	