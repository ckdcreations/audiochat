#include <stdio.h>
#include "portaudio.h"

#define SAMPLE_RATE (48000)

/* typedef struct: paTestData
*/
typedef struct 
{
    float leftPhase;
    float rightPhase;
}
paTestData;


static int PaTestCallback( 
                            const void *inputBuf, 
                            void *outputBuf, 
                            unsigned long framesPerBuf, 
                            const PaStreamCallbackTimeInfo* timeInfo, 
                            PaStreamCallbackFlags statusFlags, 
                            void *userData 
                          )
{
    // Cast data passed through stream to our structure. 
    paTestData *data = (paTestData*)userData;
    float *out = (float*)outputBuf;
    unsigned int i;
    (void) inputBuf; // Prevent unused variable warning. 

    for( i = 0; i < framesPerBuf; i++ )
    {
        *out++ = data->leftPhase;
        *out++ = data->rightPhase;
        // Generate simple sawtooth phaser that ranges between -1.0 and 1.0. 
        data->leftPhase += 0.01f;
        // When signal reaches top, drop back down. 
        if( data->leftPhase >= 1.0f  ) 
            data->leftPhase -= 2.0f;
        // Right channel higher pitch for distinguishing left and right. 
        data->rightPhase += 0.03f;
        if( data->rightPhase >= 1.0f ) 
            data->rightPhase -= 2.0f;
    }


    return 0;
}

int main()
{

    static paTestData data;
    PaStream *stream;
    PaError err = Pa_Initialize();

    if( err != paNoError  )
        printf( "PortAudio error: %s\n", Pa_GetErrorText( err ) );
    else
        printf( "PortAudio Initialized\n" );

    err = Pa_OpenDefaultStream( 
                                &stream,
                                0,
                                2,
                                paFloat32,
                                SAMPLE_RATE,
                                256,
                                PaTestCallback,
                                &data
                                );

    if( err != paNoError )
        printf( "PortAudio error: %s\n", Pa_GetErrorText( err ) );
    else
        printf( "PortAudio Stream Open\n" );


    err = Pa_StartStream( stream );
    if( err != paNoError  )
        printf( "PortAudio error: %s\n", Pa_GetErrorText( err ) );
    else
        printf( "PortAudio Stream Started\n" );

    
    Pa_Sleep(5000);

    err = Pa_StopStream( stream );
    if( err != paNoError )
        printf( "PortAudio error: %s\n", Pa_GetErrorText( err ) );
    else
        printf( "PortAudio Stream Stopped\n" );

    err = Pa_CloseStream( stream );
    if( err != paNoError )
        printf( "PortAudio error: %s\n", Pa_GetErrorText( err ) );
    else
        printf( "PortAudio Stream Closed\n" );

    

    err = Pa_Terminate();
    if( err != paNoError )
        printf( "PortAudio error: %s\n", Pa_GetErrorText( err ) );
    else
        printf( "PortAudio Terminated\n" );

    return 0;
}
